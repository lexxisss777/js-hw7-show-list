
const myHeroesArray = ["Batman", "Superman", "Iron-man", "Boogy-man", "Flash-man", "Captain-America", "Spider-man"];
const myHeroesFunction = (array) => {
    const list = document.createElement("ul");
    const listElements = array.map(elem => `<li>${elem}</li>`);
    list.innerHTML = listElements.join("");
    document.body.append(list);

};
myHeroesFunction(myHeroesArray);